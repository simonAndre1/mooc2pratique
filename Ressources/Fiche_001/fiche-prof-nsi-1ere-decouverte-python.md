# Découverte de Python via le module `turtle`

- **Fiche élève cours :** _non fournie_
- **Fiche élève activité :** [fiche-eleve-nsi-1ere-decouverte-python.pdf](fiche-eleve-nsi-1ere-decouverte-python.pdf)
- **Déroulé :** [deroule-nsi-1ere-decouverte-python.md](deroule-nsi-1ere-decouverte-python.md)

--- 

**Thématique :** notions transversales de programmation.

**Notions liées :** affectations, variables, séquences, instructions conditionnelles, boucles bornées et non bornées, définitions et appels de fonctions, écrire et développer des programmes pour répondre à des problèmes et modéliser des phénomènes physiques, économiques et sociaux.

**Résumé de l’activité :** voir les notions de bases en programmation
en utilisant le module turtle de python pour dessiner.

**Objectifs :** présenter la syntaxe python, la programmation séquentielle et textuelle.

**Auteur :** Maxime Fourny <maxime-simon.fourny@ac-besancon.fr>

**Durée de l’activité :** 3H peut être faite en coordination avec le
cours de mathématiques.

**Forme de participation :** individuelle en autonomie

**Matériel nécessaire :** ordinateur avec installé un interprète Python et le module `Turtle`

**Préparation :** aucune

