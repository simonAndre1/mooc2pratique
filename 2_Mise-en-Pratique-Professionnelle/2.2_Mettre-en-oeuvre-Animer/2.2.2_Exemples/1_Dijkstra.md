# Manipulation de graphes, recherche de chemin

Une erreur fréquemment observée chez un enseignant stagiaire est de le voir reproduire un schéma vécu dans ces dernières années d'études : le cours magistral.

Cette stratégie pédagogique nécessite de la part des élèves une certaine maturité et une relation aux savoirs déjà bien établie.

Il est tout à fait possible de mettre en place d'autres stratégies où l'élève est davantage acteur de ses apprentissages : c'est en produisant qu'il progressera et apprendra les notions, les concepts, les méthodes.

L'activité présentée ici est une <a href="https://eduscol.education.fr/document/23503/download" target="_blank">activité issue d'EDUSCOL</a>.

Il s'agit de déterminer le plus court chemin sur un graphe (en présentant l'algorithme de Dijkstra). Au-delà de l'algorithme, cette activité permet de montrer l'universalité de l'informatique : un même algorithme pour différents champs d'application (plus court chemin entre deux villes, algorithme de routage).


Vous trouverez ici :

- [L'activité élève](https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/2_Mettre-en-oeuvre-Animer/fiche_eleve_dijkstra.pdf?inline=false)
- Une <a href="https://youtu.be/dg2Ys3DHbQE" target="_blank">vidéo présentant les intentions de l'auteur</a> et une présentation succinte du déroulé de la séance.
- Un [diaporama](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/raw/master/2_Mise-en-Pratique-Professionnelle/2.2_Mettre-en-oeuvre-Animer/2.2.2_Exemples/dijkstra.pdf?inline=false) présentant pas à pas l'algorithme de Dijkstra
- Une <a href ="https://www.youtube.com/watch?v=9YBq5bcw1pU" target="_blank">vidéo de présentation de ce diaporama</a>


Cette activité que l'on peut proposer à des élèves de seconde dans le cadre de la SNT ou en terminale au moment de la séquence sur les algorithmes de routage fait manipuler l'algorithme de Dijkstra par les élèves. Il ne s'agit nullement de le programmer. Cela est beaucoup plus compliqué et nécessite de nombreux pré-requis (représentation des graphes, file...).

L'élève est amené à appliquer l'algorithme de Dijkstra graphiquement sur le graphe. Il dispose pour s'aider de la<a href="https://www.youtube.com/watch?v=9YBq5bcw1pU" target="_blank"> vidéo mentionnée ci-dessus</a> qu'il peut à loisir mettre en pause.

Sans parler de classe inversée, la stratégie pédagogique est de lui mettre à disposition des documents qu'il peut analyser pour questionner sa pratique. Ces documents sont ensuite mis à sa disposition (moodle par exemple) et il peut les consulter à loisir.


Nous pouvons échanger sur cet exemple dans <a href="https://mooc-forums.inria.fr/moocnsi/t/introduction-a-lalgorithme-de-dijkstra/3467" target="_blank">cette discussion dédiée du forum</a>.




