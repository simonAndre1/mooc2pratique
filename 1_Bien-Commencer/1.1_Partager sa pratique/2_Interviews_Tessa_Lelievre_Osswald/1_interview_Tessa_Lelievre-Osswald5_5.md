# Interview Tessa Lelièvre-Osswald 5/5, enseigner l'informatique au secondaire

Cette série d'interviews a pour but de recueillir l'expérience d'enseignants et d'enseignantes de NSI (spécialité Numérique et Sciences Informatiques):
comment sont-ils arrivés à enseigner cette discipline, quelles difficultés ont-ils rencontrées, quelle pédagogie ont-ils mis en oeuvre, ...  ? Ils et elles témoignent pour partager leurs pratiques avec de jeunes venus dans l'enseignement de NSI.

## Interview de Tessa Lelièvre-Osswald, enseignante en NSI

**Sommaire des 5 vidéos**

* 1/5 [1/5 Tessa Lelièvre-Osswald, qui es-tu et quel est ton parcours ?](./1_interview_Tessa_Lelievre-Osswald1_5.md) 
* 2/5 [Comment se former et préparer ses cours ?](./1_interview_Tessa_Lelievre-Osswald2_5.md) 
* 3/5 [Pratique en classe\.](./1_interview_Tessa_Lelievre-Osswald3_5.md)
* 4/5 [Enseignement adapté aux différents élèves\.](./1_interview_Tessa_Lelievre-Osswald4_5.md)
* **5/5 Conseils aux autres collègues\.](./1_interview_Tessa_Lelievre-Osswald5_5.md)**


## 5/5 Conseils aux autres collègues 

_"C’est très important déjà de savoir qu'il faut continuer à apprendre : par exemple, moi-même, je n'avais pas été formée à tout ce qui est hardware [à contrario] de mes études très théoriques ; J'ai dû me former par moi-même et en discutant avec d'autres collègues ..."_

[![Interview Tessa Lelièvre-Osswald 5/5](https://mooc-nsi-snt.gitlab.io/portail/assets/vignette-interview-Tessa-600x311.png)](https://files.inria.fr/LearningLab_public/C045TV/ITW/NSI-ITW-TL-5.mp4)


### Quelques conseils aux futurs collègues

_Alors maintenant, quels conseils donner aux autres collègues pour se former à NSI ?_

Déjà, c'est très important de se rendre compte qu'il va falloir continuer à apprendre puisque même moi, je devais par exemple pas étudier tout ce qui est hardware au fur à mesure de mes études, qui étaient très théoriques, mais il y en a au programme, donc j'ai dû me former par moi même, et en discutant avec d'autres d'autres collègues. Et donc pour ça, comme ressource, j'ai eu aussi bien à l'[INSPÉ](https://fr.wikipedia.org/wiki/Institut_national_sup%C3%A9rieur_du_professorat_et_de_l%27%C3%A9ducatio) où on m'a proposé, notamment, des cours de robotique (qui n'étaient pas prévu au programme) mais m'ont aidé à être un peu plus dans le hardware. Donc ne pas hésiter à aller vers ce genre de piste.

_Et pour préparer le CAPES. Quels conseils ?_

Alors ben si vous pensez que la manière dont j'ai fait est bien : préparer ses cours à l'avance. Et pour la partie formation pédagogique, personnellement, je n'en ai pas eu. J'y suis allé comme ça .. au talent, pourrait-on dire :) Mais j'ai pu aussi en discuter un peu avec mes enseignants de l'université qui m'ont donné quelques conseils.

_Et maintenant, quels conseils tu pourrais leur donner pour la préparation des cours ?_

Je dirais que c'est important d'avoir une idée assez claire de ce qu'on va faire sur un cours, puisque le programme est quand même très dense par rapport aux heures qu'on a, donc avoir vraiment .. le plan de ce qu'on va dire et de ce qu'on va faire comme exemple, pour pouvoir le faire sereinement au niveau du temps.

_D'accord, et au niveau de la gestion de la classe. Quel serait ton conseil ?_

Pour ma part, j'ai fait le choix, contrairement à certains autres enseignants qui font différemment, d'éviter au maximum tout ce qui est sanction ou punition. Puisque j'aime avoir cette cette confiance avec les élèves. Je me dis ayant des élèves qui ont 17, 18 ans, si je les punis, je ne suis pas sûr que ça va leur apprendre quelque chose ou même cela risque de les braquer et qu'ils n'aient plus envie de travailler. Donc, je fais ce choix d'essayer au maximum de discuter avec eux et cette année, je n'ai mis aucune punition, aucune sanction, notamment. Par exemple, j'ai un élève qui a pensé que c'était rigolo de fumer une cigarette électronique dans sa trousse. Ce n'est pas tout à fait une situation à laquelle on s'attend quand on devient prof, surtout au début. Mais je lui ai juste dit, mais qu'est ce que tu es en train de faire ... quoi ? Et puis, tout le monde a rigolé puisque ça lui mettait un peu la honte que je le dise. Et puis voilà, ça s'est arrêté là, sans avoir besoin de le coller ni quoi que ce soit, ce qui fait que par la suite, il 'a pas recommencé : on n'a pas été en conflit. Donc ça s'est bien passé toute l'année.







