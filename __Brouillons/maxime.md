## récupérer et s'approprier
identifier les objectifs en terme de connaissance
identifier les objectifs en terme de compétences
identifier les pré requis nécessaire

## anticiper et étayer
identifier les démarches potentielles des élèves
identifier les obstacles prévisibles
anticiper un étayage de ces difficultés
anticiper des aides

## scénariser une séance
décliner le déroulement d'une séance : différentes phase - durée...
préciser l'organisation du tableau - écran - papier
préciser l'organisation pédagogique (seul, binône, groupe...)
préciser les attendus
préciser les interactions
préciser l'activité du professeur
détailler la place de la trace écrite

## Construire une activité élève de compréhension

## Construire une activité de production

## Construire une séquence

## gérer l'hétérogénéité

## Analyser réflexivement une séance