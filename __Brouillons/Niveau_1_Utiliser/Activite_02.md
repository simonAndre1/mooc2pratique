# Adapter des activités 

En puisant dans les ressources existantes à votre disposition, et en vous inspirant de la fiche exemple sur la programmation ainsi que sur l'activité 1, présentez une activité pour un des thèmes du programme NSI Première ou Terminale en donnant :

- la fiche _élève_ (la ressource que vous aurez trouvée, plus ou moins adaptée)
- la fiche _prof_