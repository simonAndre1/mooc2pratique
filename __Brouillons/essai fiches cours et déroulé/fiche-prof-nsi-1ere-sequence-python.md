## Exemple de méta-données de fiche prof pour une activité

**Thématique :** Apprentissage de la programmation

**Notions liées :** séquences d'instructions, tuples, fonctions, tableaux, listes, boucles, tableaux 2D.

**Résumé de l’activité :** Manipuler les structures de base de python en lien avec les séquences avec des exercices à trou.

**Objectifs :** Familiariser avec les contsructions de base du langage Python

**Auteur :** David Roche <"dav74130" <dav74130@gmail.com>; >

**Durée de l’activité :** une heure `A PRECISER`

**Forme de participation :** individuelle, en autonomie, sur machine.

**Matériel nécessaire :** ordinateur avec installé un interprète Python

**Préparation :** aucune

### Références:

**Fiche élève cours :** https://pixees.fr/informatiquelycee/prem/c2c.html

**Fiche élève activité :** https://pixees.fr/informatiquelycee/prem/c2a.html

**Déroulé :** ./deroule-nsi-1ere-sequence-python.md


